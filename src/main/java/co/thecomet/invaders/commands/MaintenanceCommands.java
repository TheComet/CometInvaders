package co.thecomet.invaders.commands;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.invaders.Invaders;
import co.thecomet.invaders.config.InvadersConfig;
import co.thecomet.invaders.config.InvadersMapConfig;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.phases.MaintenancePhase;
import co.thecomet.invaders.game.phases.PreGamePhase;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class MaintenanceCommands {
    public MaintenanceCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "setspawn", Rank.ADMIN, MaintenanceCommands::setSpawn);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "minplayers", Rank.ADMIN, MaintenanceCommands::setMinPlayers);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "mm", Rank.ADMIN, MaintenanceCommands::maintenance);
    }

    public static void setSpawn(Player player, String[] args) {
        if (player.getWorld() != Bukkit.getWorlds().get(0)) {
            MessageFormatter.sendErrorMessage(player, "You must be in the main world!");
            return;
        }
        
        InvadersConfig config = Invaders.getInstance().getConfiguration();
        config.setSpawn(new JsonLocation(player));
        Invaders.getInstance().saveConfiguration();
        MessageFormatter.sendSuccessMessage(player, "You have set the spawn to your location.");
    }

    public static void setMinPlayers(CommandSender sender, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(sender, "/minplayers <# of players>");
            return;
        }

        try {
            int min = Integer.parseInt(args[0]);
            InvadersConfig config = Invaders.getInstance().getConfiguration();
            config.setPlayerMinOverride(min);
            Invaders.getInstance().saveConfiguration();
            MessageFormatter.sendSuccessMessage(sender, "You have set the minimum players to " + min);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/minplayers <# of players>");
            return;
        }
    }

    /**
     * Command that enables/disables maintenance mode and
     * handles all necessary command registration.
     * *
     * @param sender
     * @param args
     */
    public static void maintenance(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        Boolean val;
        try {
            val = Boolean.parseBoolean(args[0]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        if (val.booleanValue()) {
            if (GameLoop.getInstance().getPhase() instanceof MaintenancePhase) {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is already enabled!");
            } else {
                if (!(GameLoop.getInstance().getPhase() instanceof PreGamePhase)) {
                    MessageFormatter.sendErrorMessage(sender, "Maintenance can only be enabled during pregame phase!");
                    return;
                }
                
                GameLoop.getInstance().setPhase(new MaintenancePhase());
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpw", Rank.ADMIN, MaintenanceCommands::tpw);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "conf", Rank.ADMIN, MaintenanceCommands::conf);
                CommandRegistry.registerPlayerSubCommand("conf", "setmapname", Rank.ADMIN, MaintenanceCommands::setMapName);
                CommandRegistry.registerPlayerSubCommand("conf", "setauthorname", Rank.ADMIN, MaintenanceCommands::setAuthorName);
                CommandRegistry.registerPlayerSubCommand("conf", "ats", Rank.ADMIN, MaintenanceCommands::addTeamSpawn);
                CommandRegistry.registerPlayerSubCommand("conf", "sdts", Rank.ADMIN, MaintenanceCommands::setDefaultTeamSpawn);
                CommandRegistry.registerPlayerSubCommand("conf", "cts", Rank.ADMIN, MaintenanceCommands::clearTeamSpawns);


                for (Player player : Bukkit.getOnlinePlayers()) {
                    NetworkPlayer bp = CoreAPI.getPlayer(player);
                    if (bp == null || bp.getRank().ordinal() < Rank.MOD.ordinal()) {
                        player.kickPlayer("\nMaintenance mode has been enabled.\nWe are sorry for the inconvenience.");
                    } else {
                        player.setGameMode(GameMode.CREATIVE);
                        player.getInventory().clear();
                        player.updateInventory();
                    }
                }
            }
        } else {
            if (GameLoop.getInstance().getPhase() instanceof MaintenancePhase) {
                GameLoop.getInstance().setPhase(GameLoop.getInstance().getPhase().next());
                CommandRegistry.unregisterCommand("tpw");
                CommandRegistry.unregisterCommand("conf");

                GameLoop.getInstance().teleportAllToLobby();
            } else {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is not enabled!");
            }
        }
    }

    /**
     * Command to teleport a player to another world.
     * * 
     * @param sender
     * @param args
     */
    public static void tpw(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/tpw <world>");
            return;
        }
        
        if (Bukkit.getWorld(args[0]) != null) {
            if (Bukkit.getWorlds().get(0).equals(Bukkit.getWorld(args[0]))) {
                sender.teleport(Invaders.getInstance().getConfiguration().getSpawn().getLocation());
                return;
            }
        }

        MapManager<InvadersMapConfig> manager = GameLoop.getInstance().getMapManager();
        String world = args[0];
        for (File dir : manager.getAllWorlds().values()) {
            if (dir.getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName())) {
                continue;
            }

            if (dir.getName().equalsIgnoreCase(world)) {
                InvadersMapConfig config = JsonConfig.load(new File(dir, "map.json"), InvadersMapConfig.class);
                config.save(dir);
            } else {
                continue;
            }
            
            World destination = null;
            for (World w : Bukkit.getWorlds()) {
                if (w.getName().equalsIgnoreCase(world)) {
                    destination = w;
                }
            }
            
            if (destination == null) {
                File destDir = new File(Bukkit.getWorldContainer(), dir.getName());
                try {
                    if (destDir.exists()) {
                        FileUtils.deleteDirectory(destDir);
                    }

                    FileUtils.copyDirectory(dir, destDir);
                } catch (IOException e) {
                    MessageFormatter.sendErrorMessage(sender, "Failed to copy the file from the maps folder.");
                    return;
                }
                
                for (File file : destDir.listFiles()) {
                    if (file.getName().equalsIgnoreCase("uid.dat") || file.getName().equalsIgnoreCase("session.lock")) {
                        file.delete();
                    }
                }
                
                destination = new WorldCreator(dir.getName())
                        .generateStructures(false)
                        .environment(World.Environment.NORMAL)
                        .type(WorldType.NORMAL)
                        .generator(new VoidGenerator())
                        .createWorld();
                destination.setAutoSave(false);
                
                for (Entity entity : destination.getEntities()) {
                    if ((entity instanceof Player) == false) {
                        entity.remove();
                    }
                }
            }
            
            sender.teleport(destination.getSpawnLocation());
            MessageFormatter.sendSuccessMessage(sender, "You have been teleported to &6" + destination.getName());
            return;
        }
        
        MessageFormatter.sendErrorMessage(sender, "That world does not exists!");
    }

    /**
     * Main command for map configuration commands.
     * *
     * @param player
     * @param args
     */
    public static void conf(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setspectatorspawn");
        MessageFormatter.sendUsageMessage(player, "/conf setarenacenter");
        MessageFormatter.sendUsageMessage(player, "/conf addpedestal");
        MessageFormatter.sendUsageMessage(player, "/conf clearpedestals");
        MessageFormatter.sendUsageMessage(player, "/conf tpvalidatepedestals");
        MessageFormatter.sendUsageMessage(player, "/conf detectchests <radius>");
    }
    
    public static void setMapName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
            return;
        }
        
        LoadedMap<InvadersMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }
        
        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        map.getConfig().mapName = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "You have set the map name to &6" + map.getMapName());
    }
    
    public static void setAuthorName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
            return;
        }

        LoadedMap<InvadersMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().mapAuthor = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "You have set the map author to &6" + map.getMapAuthor());
    }
    
    public static void addTeamSpawn(Player player, String[] args) {
        LoadedMap<InvadersMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/conf ats <team name>");
            return;
        }

        for (String name : map.getConfig().getTeamSettings().keySet()) {
            if (name.equalsIgnoreCase(args[0])) {
                MessageFormatter.sendSuccessMessage(player, "Spawn point for " + name + " added at your position.");
                map.getConfig().getTeamSettings().get(name).getSpawnPoints().add(new JsonLocation(player));
                map.getConfig().save(new File(map.getWorld(), "map.json"));
                return;
            }
        }

        MessageFormatter.sendErrorMessage(player, "That is not a valid team.");
    }

    public static void setDefaultTeamSpawn(Player player, String[] args) {
        LoadedMap<InvadersMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/conf sdts <team name>");
            return;
        }

        for (String name : map.getConfig().getTeamSettings().keySet()) {
            if (name.equalsIgnoreCase(args[0])) {
                MessageFormatter.sendSuccessMessage(player, "Default spawn point for " + name + " set to your position.");
                map.getConfig().getTeamSettings().get(name).getSpawnPoints().add(new JsonLocation(player));
                map.getConfig().save(new File(map.getWorld(), "map.json"));
                return;
            }
        }

        MessageFormatter.sendErrorMessage(player, "That is not a valid team.");
    }
    
    public static void clearTeamSpawns(Player player, String[] args) {
        LoadedMap<InvadersMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/conf cts <team name>");
            return;
        }

        for (String name : map.getConfig().getTeamSettings().keySet()) {
            if (name.equalsIgnoreCase(args[0])) {
                MessageFormatter.sendSuccessMessage(player, "Spawn points for " + name + " have been cleared.");
                map.getConfig().getTeamSettings().get(name).getSpawnPoints().clear();
                map.getConfig().save(new File(map.getWorld(), "map.json"));
                return;
            }
        }

        MessageFormatter.sendErrorMessage(player, "That is not a valid team.");
    }
}
