package co.thecomet.invaders.utils.nms;

import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityLiving;

import java.lang.reflect.Field;

public class EntityUtil {
    // Living Entity Fields
    private static Field FIELD_JUMP = null;

    public static boolean isJumping(Entity entity) {
        if (FIELD_JUMP == null) {
            try {
                FIELD_JUMP = EntityLiving.class.getDeclaredField("aY");
                FIELD_JUMP.setAccessible(true);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        if (FIELD_JUMP != null && entity instanceof EntityLiving) {
            try {
                return FIELD_JUMP.getBoolean(entity);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
