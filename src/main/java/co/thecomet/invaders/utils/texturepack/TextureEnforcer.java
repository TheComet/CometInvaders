package co.thecomet.invaders.utils.texturepack;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.invaders.Invaders;
import co.thecomet.invaders.config.InvadersConfig;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TextureEnforcer implements Listener {
    private ProtocolManager protocolManager;

    public TextureEnforcer() {
        protocolManager = ProtocolLibrary.getProtocolManager();
        Bukkit.getPluginManager().registerEvents(this, CoreAPI.getPlugin());

        protocolManager.addPacketListener(new PacketAdapter(CoreAPI.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Client.RESOURCE_PACK_STATUS) {
            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.RESOURCE_PACK_STATUS) {
                    EnumWrappers.ResourcePackStatus status = event.getPacket().getResourcePackStatus().read(0);
                    final Player player = Bukkit.getPlayer(event.getPlayer().getName());
                    if (status.toString().equalsIgnoreCase("DECLINED")) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(FontColor.translateString("&6You must &4accept the server pack &6!")));
                    } else if (status.toString().equalsIgnoreCase("FAILED_DOWNLOAD")) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(FontColor.translateString("&cFailed to download the texture pack!")));
                    }
                }
            }
        });
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        InvadersConfig config = Invaders.getInstance().getConfiguration();

        if (config.getTexturePack() == null || config.getTexturePack().isEmpty()) {
            return;
        }

        final Player p = event.getPlayer();
        p.setTexturePack(config.getTexturePack());
    }
}
