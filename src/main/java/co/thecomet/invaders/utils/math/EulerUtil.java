package co.thecomet.invaders.utils.math;

import org.bukkit.Location;
import org.bukkit.util.EulerAngle;

public class EulerUtil {
    public static EulerAngle fromPitchYaw(float pitch, float yaw, float standYaw) {
        return new EulerAngle(Math.toRadians(pitch), Math.toRadians(yaw - standYaw), Math.toRadians(360.0D));
    }

    public static EulerAngle fromLocation(Location location, float standYaw) {
        return fromPitchYaw(location.getPitch(), location.getYaw(), standYaw);
    }
}
