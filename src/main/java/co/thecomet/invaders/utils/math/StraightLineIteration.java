package co.thecomet.invaders.utils.math;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class StraightLineIteration {

    /**
     * Will get the line of sight of a player in a {@link LinkedList}
     *
     * @param player The player which is going to be used.
     * @param range The range of locations where we're going to iterate.
     * @return
     */
    public static List<Location> inLine(Player player, int range) {
        List<Block> blocks = player.getLineOfSight((HashSet<Byte>) null, range);
        return blocks.stream().map(Block::getLocation).collect(Collectors.toCollection(LinkedList::new));
    }

    private static class LocationIterator implements Iterator<Location> {

        private final int realSize;
        private int index = 0;
        private List<Location> locations;

        public LocationIterator(List<Location> locations) {
            this.locations = locations;
            this.index = 0;
            this.realSize = locations.size();
        }

        @Override
        public boolean hasNext() {
            return index <= realSize;
        }

        @Override
        public Location next() {
            return locations.get(index++);
        }
    }

}
