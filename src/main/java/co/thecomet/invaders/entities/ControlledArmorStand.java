package co.thecomet.invaders.entities;

import co.thecomet.invaders.entities.nms.EntityControlledArmorStand;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.entity.CreatureSpawnEvent;

public interface ControlledArmorStand extends Vehicle, ArmorStand {
    public static ControlledArmorStand spawn(Location location) {
        CraftWorld cw = CraftWorld.class.cast(location.getWorld());
        EntityControlledArmorStand as = new EntityControlledArmorStand(cw.getHandle());
        as.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), 0.0F);
        cw.getHandle().addEntity(as, CreatureSpawnEvent.SpawnReason.CUSTOM);
        return ControlledArmorStand.class.cast(as.getBukkitEntity());
    }

    public void setSize(float width, float length);
}
