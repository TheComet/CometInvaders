package co.thecomet.invaders.entities.nms;

import co.thecomet.invaders.entities.craft.CraftControlledArmorStand;
import co.thecomet.invaders.utils.nms.EntityUtil;
import co.thecomet.invaders.utils.math.EulerUtil;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;

public class EntityControlledArmorStand extends EntityArmorStand {
    public EntityControlledArmorStand(World world) {
        super(world);
    }

    @Override
    public CraftEntity getBukkitEntity() {
        if(this.bukkitEntity == null) {
            this.bukkitEntity = new CraftControlledArmorStand(this.world.getServer(), this);
        }

        return this.bukkitEntity;
    }

    @Override
    public void t_() {
        super.t_();
    }

    @Override
    public void g(float f, float f1) {
        if (this.passenger == null || !(this.passenger instanceof EntityPlayer)) {
            return;
        }

        Player p = (Player) this.passenger.getBukkitEntity();
        if (p != null && p.isOnline()) {
            EntityPlayer player = ((CraftPlayer) p).getHandle();

            this.lastYaw = this.yaw = player.yaw;
            this.pitch = player.pitch * 0.5F;
            this.setYawPitch(this.yaw, this.pitch);
            this.aK = this.aI = this.yaw;
            // side motion
            f = player.aZ * 0.5F;
            // forward motion
            f1 = player.ba;

            if (f1 <= 0.0F) {
                f1 *= 0.5F;
            }

            if (f != 0.0F || f1 != 0.0F) {
                this.motY = p.getLocation().getDirection().getY();
            } else {
                motY = 0.0D;
            }

            if (f1 < 0.0F || (f != 0.0F && f1 == 0.0F)) {
                motY = 0.0D;
            }

            if (this.passenger != null && this.motY == 0.0D) {
                if (this.passenger.isSneaking()) {
                    motY -= 0.3;
                } else if (EntityUtil.isJumping(player)) {
                    motY += 0.3;
                }
            }

            this.S = 1.0F;
            this.aM = this.bI() * 0.1F;

            if (!this.world.isClientSide) {
                this.k((float) this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue());
                super.g(f, f1);
            }

            this.aA = this.aB;
            double d0 = this.locX - this.lastX;
            double d1 = this.locZ - this.lastZ;
            float f4 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;

            if (f4 > 1.0F) {
                f4 = 1.0F;
            }

            this.aB += (f4 - this.aB) * 0.4F;
            this.aC += this.aB;

            ArmorStand armorStand = (ArmorStand) this.getBukkitEntity();
            EulerAngle euler = EulerUtil.fromLocation(p.getLocation(), yaw);
            double maxBound = Math.toRadians(50.0F);
            double minBound = Math.toRadians(-50.0F);
            if (euler.getX() > maxBound) {
                euler = euler.setX(maxBound);
            } else if (euler.getX() < minBound) {
                euler = euler.setX(minBound);
            }
            armorStand.setHeadPose(euler);
        }
    }

    @Override
    public void m() {
        if (this.passenger == null || !(this.passenger instanceof EntityPlayer)) {
            return;
        }

        Player p = (Player) this.passenger.getBukkitEntity();
        if (p != null && p.isOnline()) {
            EntityPlayer player = ((CraftPlayer) p).getHandle();
            g(player.aZ, player.ba);
        }
    }
}
