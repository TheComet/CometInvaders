package co.thecomet.invaders.entities.craft;

import co.thecomet.invaders.entities.ControlledArmorStand;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArmorStand;

public class CraftControlledArmorStand extends CraftArmorStand implements ControlledArmorStand {
    public CraftControlledArmorStand(CraftServer server, EntityArmorStand entity) {
        super(server, entity);
    }

    @Override
    public void setSize(float width, float length) {
        this.getHandle().setSize(width, length);
    }
}
