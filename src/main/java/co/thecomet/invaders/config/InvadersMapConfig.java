package co.thecomet.invaders.config;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.minigameapi.config.BaseConfig;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InvadersMapConfig extends BaseConfig {
    @Getter @Setter
    private int roundTime = 300;
    @Getter
    private Map<String, TeamSettings> teamSettings = new HashMap<String, TeamSettings>(){{
        put("alpha", new TeamSettings());
        put("bravo", new TeamSettings());
    }};

    public class TeamSettings {
        @Getter @Setter
        private JsonLocation defaultTeamSpawn;
        @Getter
        private List<JsonLocation> spawnPoints = new ArrayList<>();
    }
}
