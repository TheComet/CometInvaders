package co.thecomet.invaders.config;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.core.config.JsonLocation;
import lombok.Getter;
import lombok.Setter;

public class InvadersConfig extends JsonConfig {
    @Getter @Setter
    private int playerMinOverride = 0;
    @Getter @Setter
    private JsonLocation spawn = new JsonLocation("world", 0, 256, 0, 0.0F, 0.0F);
    @Getter @Setter
    private String texturePack = "";
}
