package co.thecomet.invaders.mechanics;

import co.thecomet.invaders.game.ship.Ship;

public interface Powerup {
    public void apply(Ship ship);
}
