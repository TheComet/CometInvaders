package co.thecomet.invaders.mechanics.powerups;

import co.thecomet.invaders.game.players.Participant;
import co.thecomet.invaders.game.ship.Ship;
import co.thecomet.invaders.mechanics.Powerup;

public class ShieldPowerup implements Powerup {

    private double powerup = 0.0D;
    private int seconds = 20;

    public ShieldPowerup(double powerup, int seconds) {
        this.powerup = powerup;
        this.seconds = seconds;
    }

    public void apply(Ship ship) {
        if (ship.getActivePowerup() instanceof ShieldPowerup) {
            return;
        }

        // TODO: xupdate player absorption
        /*if (ship.getShield().getCurrentHealth() < 0) {
            //This shouldn't happen, but w/e.
            return;
        }

        final double prePowerup = ship.getShield().getCanHold();
        ship.getShield().setCanHold(ship.getShield().getCanHold() + powerup);

        new BukkitRunnable() {
            @Override
            public void run() {
                if (ship.getShield().getCurrentHealth() > 0) {
                    ship.getShield().setCanHold(prePowerup);
                }
            }
        }.runTaskLater(Invaders.getPlugin(Invaders.class), seconds * 20L);*/
    }
}
