package co.thecomet.invaders.mechanics.powerups;

import co.thecomet.invaders.Invaders;
import co.thecomet.invaders.game.players.Participant;
import co.thecomet.invaders.game.ship.Ship;
import co.thecomet.invaders.mechanics.Powerup;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class SpeedPowerup implements Powerup {

    private double speedMultiplier;
    private int seconds;

    public SpeedPowerup(double speedMultiplier, int seconds) {
        this.speedMultiplier = speedMultiplier;
        this.seconds = seconds;
    }

    public void apply(Ship ship) {
        if (ship.getActivePowerup() instanceof SpeedPowerup) {
            return;
        }

        double initSpeed = ship.getSpeed();
        ship.setSpeed(speedMultiplier);
        new BukkitRunnable() {
            @Override
            public void run() {
                ship.setSpeed(initSpeed);
            }
        }.runTaskLater(Invaders.getPlugin(Invaders.class), seconds * 20L);
    }
}
