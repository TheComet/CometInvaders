package co.thecomet.invaders.game.ui;

import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.classes.GameClass;
import co.thecomet.invaders.game.classes.Tank;
import co.thecomet.invaders.game.phases.PreGamePhase;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Map;

public class ClassSelectionMenu extends Menu {
    private static Map<Class<? extends GameClass>, GameClass> gameClasses;
    private MenuItem item;
    
    public ClassSelectionMenu() {
        super("Class Selection", 6);
        gameClasses = GameLoop.getInstance().getGameClasses();
        item = new MenuItem("Class Selection", new MaterialData(Material.IRON_AXE)) {
            @Override
            public void onClick(Player player) {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase) {
                    openMenu(player);
                }
            }
        };
        init();
    }
    
    private void init() {
        gameClasses.putIfAbsent(Tank.class, new Tank());
        
        for (GameClass gameClass : new ArrayList<>(gameClasses.values())) {
            addMenuItem(gameClass.getMenuItem(), gameClass.getMenuItem().getSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
