package co.thecomet.invaders.game;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.invaders.config.InvadersMapConfig;
import co.thecomet.invaders.game.team.Team;
import co.thecomet.minigameapi.world.LoadedMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class GameSession {
    @Getter
    private LoadedMap<InvadersMapConfig> map;
    @Getter
    private UUID mapUuid;
    @Getter
    private List<Team> teams = Lists.newArrayList();
    @Getter
    public Map<UUID, JsonLocation> assignedTeamSpawns = Maps.newHashMap();

    public GameSession(LoadedMap<InvadersMapConfig> map, UUID mapUuid) {
        this.map = map;
        this.mapUuid = mapUuid;
    }

    public World getWorld() {
        if (mapUuid != null) {
            return Bukkit.getWorld(mapUuid);
        } else if (map != null) {
            return Bukkit.getWorld(map.getWorld().getName());
        } else {
            return null;
        }
    }
}
