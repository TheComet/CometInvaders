package co.thecomet.invaders.game.ship;

import co.thecomet.invaders.entities.ControlledArmorStand;
import co.thecomet.invaders.mechanics.Powerup;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Ship {
    @Getter
    private Material block;
    @Getter
    private short data;
    @Getter @Setter
    private float power;
    @Getter @Setter
    private double shipHealth;
    @Getter @Setter
    private double speed;
    @Getter @Setter
    private double damageOutput;
    @Getter
    private Powerup activePowerup;
    @Getter
    private ControlledArmorStand entity;

    public Ship(Material block, short data) {
        this.block = block;
        this.data = data;
    }

    public void mountPlayer(Player player) {
        this.entity = ControlledArmorStand.spawn(player.getLocation());
        this.entity.setSize(3.0F, 3.0F);
        this.entity.getEquipment().setHelmet(new ItemStack(block, 1, data));
        this.entity.setPassenger(player);
    }

    public void destroyShip(boolean killPassenger) {
        if (entity != null) {
            if (killPassenger) {
                killPassenger();
            }
            entity.remove();
        }
    }

    public void killPassenger() {
        if (entity.getPassenger() != null) {
            LivingEntity passenger = (LivingEntity) entity.getPassenger();
            entity.setPassenger(null);
            passenger.setHealth(0.0);
        }
    }

    public void setPowerup(Powerup powerup) {
        if (activePowerup.getClass() == powerup.getClass()) {
            return;
        }

        activePowerup = powerup;
        activePowerup.apply(this);
    }
}
