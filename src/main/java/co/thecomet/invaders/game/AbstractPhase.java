package co.thecomet.invaders.game;

public abstract class AbstractPhase implements Phase {
    private boolean complete = false;

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public void setComplete() {
        this.complete = true;
    }
}
