package co.thecomet.invaders.game.classes;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.transaction.Transaction;
import co.thecomet.core.transaction.TransactionManager;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.phases.PreGamePhase;
import co.thecomet.invaders.game.players.Participant;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseGameClass implements GameClass {
    protected MenuItem menuItem;
    protected int points = 0;
    protected Rank requiredRank = Rank.DEFAULT;
    protected Feature feature = null;
    
    public BaseGameClass(String menuDisplay, Material menuMaterial, int menuSlot) {
        menuItem = new MenuItem(menuDisplay, new MaterialData(menuMaterial)) {
            @Override
            public void onClick(Player player) {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase) {
                    NetworkPlayer bp = CoreAPI.getPlayer(player);
                    if (bp.getRank().ordinal() < getRequiredRank().ordinal()) {
                        MessageFormatter.sendErrorMessage(player, "This class requires that you be " + getRequiredRank().getDisplayName() + " or higher.");
                        return;
                    }
                    
                    if (feature != null && bp.getRank().ordinal() < Rank.ULTRA.ordinal()) {
                        boolean purchased = false;
                        for (Transaction transaction : new ArrayList<>(bp.getTransactions())) {
                            if (transaction.feature.equals(feature)) {
                                purchased = true;
                                break;
                            }
                        }
                        
                        if (!purchased) {
                            TransactionManager.submit(player, feature);
                            return;
                        }
                    }
                    
                    Participant participant = GameLoop.getInstance().getParticipants().get(player.getUniqueId());
                    if (participant.getGameClass() == BaseGameClass.this) {
                        MessageFormatter.sendErrorMessage(player, "You have already selected that class!");
                    } else {
                        participant.setGameClass(BaseGameClass.this);
                        MessageFormatter.sendSuccessMessage(player, "You have chosen to be a " + getMenuItem().getText());
                    }
                }
            }
        };
        menuItem.setSlot(menuSlot);
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public Rank getRequiredRank() {
        return requiredRank;
    }

    public void setMenuItemDescription(List<String> description) {
        List<String> colorized = description.stream().map(FontColor::translateString).collect(Collectors.toList());
        menuItem.setDescriptions(colorized);
    }
}
