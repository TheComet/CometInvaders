package co.thecomet.invaders.game.classes;

public interface ActiveGameClass {
    public void loop();
}
