package co.thecomet.invaders.game.classes;

import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.players.Participant;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class ActiveBaseGameClass extends BaseGameClass implements ActiveGameClass {
    protected ActiveLoopProcessor processor = null;
    
    public ActiveBaseGameClass(String menuDisplay, Material menuMaterial, int menuSlot) {
        super(menuDisplay, menuMaterial, menuSlot);
    }

    @Override
    public final void loop() {
        if (processor != null) {
            GameLoop.getInstance().getParticipants().values().stream().filter(participant -> participant.getGameClass() == this).forEach(participant -> {
                Player player = Bukkit.getPlayer(participant.getUuid());
                if (player != null) {
                    processor.loop(player);
                }
            });
        }
    }

    public ActiveLoopProcessor getProcessor() {
        return processor;
    }

    public void setProcessor(ActiveLoopProcessor processor) {
        this.processor = processor;
    }

    public abstract class ActiveLoopProcessor {
        public abstract void loop(Player player);
    }
}
