package co.thecomet.invaders.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.ui.MenuItem;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.List;

public interface GameClass {
    public MenuItem getMenuItem();
    
    public int getPoints();
    
    public Rank getRequiredRank();
}
