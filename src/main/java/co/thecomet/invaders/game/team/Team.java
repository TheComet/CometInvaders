package co.thecomet.invaders.game.team;

import co.thecomet.invaders.config.InvadersMapConfig;
import com.google.common.collect.Lists;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

public class Team {
    @Getter
    private String name;
    @Getter
    private List<UUID> members;
    @Getter
    private InvadersMapConfig.TeamSettings settings;

    public Team(String name, InvadersMapConfig.TeamSettings settings) {
        this.name = name;
        this.members = Lists.newArrayList();
        this.settings = settings;
    }
}
