package co.thecomet.invaders.game.players;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.classes.GameClass;
import co.thecomet.invaders.game.classes.Tank;
import co.thecomet.invaders.game.ship.Ship;
import co.thecomet.invaders.game.team.Team;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Participant {
    @Getter
    private UUID uuid;
    @Getter @Setter
    private GameClass gameClass;
    @Getter @Setter
    private Team team;
    @Getter @Setter
    private Ship currentShip;
    @Getter @Setter
    private JsonLocation spawnLocation;

    public Participant(UUID uuid) {
        this.uuid = uuid;
        this.gameClass = GameLoop.getInstance().getGameClasses().get(Tank.class);
    }

    public boolean prepareNewShip() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return false;
        }

        if (gameClass != null) {
            if (currentShip != null && !currentShip.getEntity().isDead()) {
                currentShip.destroyShip(false);
            }

            Material block = gameClass.getMenuItem().getIcon().getItemType();
            short data = gameClass.getMenuItem().getData();

            Ship ship = new Ship(block, data);
            ship.mountPlayer(player);
            setCurrentShip(ship);
            return true;
        }

        return false;
    }

    public void clean() {
        this.gameClass = GameLoop.getInstance().getGameClasses().get(Tank.class);
        this.team = null;
        this.spawnLocation = null;
        this.currentShip = null;
    }

    public void spawn() {
        if (spawnLocation != null) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                player.teleport(spawnLocation.getLocation());
            }
        }
    }
}
