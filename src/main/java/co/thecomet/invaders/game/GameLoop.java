package co.thecomet.invaders.game;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.WorldCleaner;
import co.thecomet.invaders.Invaders;
import co.thecomet.invaders.commands.MaintenanceCommands;
import co.thecomet.invaders.config.InvadersConfig;
import co.thecomet.invaders.config.InvadersMapConfig;
import co.thecomet.invaders.entities.ControlledArmorStand;
import co.thecomet.invaders.game.classes.GameClass;
import co.thecomet.invaders.game.phases.InProgressPhase;
import co.thecomet.invaders.game.phases.MaintenancePhase;
import co.thecomet.invaders.game.phases.PreGamePhase;
import co.thecomet.invaders.game.phases.StartingPhase;
import co.thecomet.invaders.game.players.Participant;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.minigameapi.world.PreMapLoadEvent;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GameLoop implements Runnable, Listener {
    @Getter
    private static GameLoop instance;

    @Getter
    private MapManager<InvadersMapConfig> mapManager;
    @Getter
    private Map<Class<? extends GameClass>, GameClass> gameClasses = new HashMap<>();
    @Getter
    private Phase phase;
    @Getter
    private Map<UUID, Participant> participants = new HashMap<>();

    public GameLoop() {
        instance = this;
        mapManager = new MapManager<>(CoreAPI.getPlugin(), InvadersMapConfig.class);

        // Register Commands
        new MaintenanceCommands();

        // Register Events
        Bukkit.getPluginManager().registerEvents(this, CoreAPI.getPlugin());
    }

    @Override
    public void run() {
        if (phase == null) {
            setPhase(new PreGamePhase());
        }

        phase.tick();

        if (phase.isComplete()) {
            setPhase(phase.next());

            phase.tick();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHangingBreak(HangingBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHangingPlace(HangingPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() instanceof ItemFrame) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || (phase instanceof InProgressPhase) == false) {
            event.setCancelled(true);
            return;
        }

        if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0)  || phase instanceof StartingPhase) {
            event.setCancelled(true);
            return;
        }

        if (event.getDamager() instanceof Player) {
            if (((Player) event.getDamager()).getGameMode() != GameMode.SURVIVAL) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof  Player || event.getEntity() instanceof ControlledArmorStand) {
            return;
        }

        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof MaintenancePhase || phase instanceof StartingPhase) {
            event.getEntity().remove();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0)) {
            event.setCancelled(true);
            return;
        }

        if (event.getEntity().getShooter() instanceof Player) {
            if (phase instanceof StartingPhase || Bukkit.getWorlds().get(0) == event.getEntity().getWorld()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(AsyncPlayerPreLoginEvent event) {
        NetworkPlayer bp = CoreAPI.getPlayer(event.getUniqueId());
        if (bp != null) {
            if (phase instanceof MaintenancePhase) {
                if (bp.getRank().ordinal() < Rank.MOD.ordinal()) {
                    event.setKickMessage("\nThis server is currently under maintenance!");
                    event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
                }
            }

            if (Bukkit.getOnlinePlayers().size() == Bukkit.getMaxPlayers() && GameLoop.getInstance().getPhase() instanceof InProgressPhase == false && bp.getRank().ordinal() > Rank.DEFAULT.ordinal()) {
                NetworkPlayer candidate = null;
                for (NetworkPlayer NetworkPlayer : CoreAPI.getNetworkPlayers()) {
                    if (NetworkPlayer.getRank() == Rank.DEFAULT) {
                        if (candidate == null) {
                            candidate = NetworkPlayer;
                            continue;
                        }

                        if (NetworkPlayer.getJoinServerTime() > candidate.getJoinServerTime()) {
                            candidate = NetworkPlayer;
                        }
                    }
                }

                if (candidate != null) {
                    final Player player = Bukkit.getPlayer(candidate.getUuid());
                    if (player != null) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(FontColor.translateString("\n&6We're sorry, but a donor has taken your place.\nYou can buy a rank at &cstore.thecomet.co")));
                    }
                    event.setLoginResult(AsyncPlayerPreLoginEvent.Result.ALLOWED);
                }
            }
        } else {
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void lowestPlayerJoin(PlayerJoinEvent event) {
        participants.put(event.getPlayer().getUniqueId(), new Participant(event.getPlayer().getUniqueId()));
        cleanPlayer(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void highestPlayerJoin(PlayerJoinEvent event) {
        if (!(phase instanceof StartingPhase || phase instanceof InProgressPhase)) {
            teleportToLobby(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void monitorPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void monitorPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);

        participants.remove(event.getPlayer().getUniqueId());

        if (Bukkit.getOnlinePlayers().size() - 1 == 0) {
            if (phase instanceof PreGamePhase == false) {
                setPhase(new PreGamePhase());
            }
        }

        WorldCleaner.clearPlayer(event.getPlayer());
    }

    @EventHandler
    public void onBlockGrow(BlockGrowEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {
        event.getWorld().setAutoSave(false);

        event.getWorld().setGameRuleValue("doMobSpawning", "false");

        for (Entity entity : event.getWorld().getEntities()) {
            if (entity instanceof Player == false) {
                entity.remove();
            }
        }
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockFade(BlockFadeEvent event) {
        if (event.getBlock().getType() == Material.ICE || event.getBlock().getType() == Material.PACKED_ICE ||
                event.getBlock().getType() == Material.SNOW || event.getBlock().getType() == Material.SNOW_BLOCK) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void preMapLoad(PreMapLoadEvent event) {
        event.getWorld().generator(new VoidGenerator());
    }

    public void setPhase(Phase phase) {
        if (this.phase != null) {
            if (this.phase instanceof Listener) {
                HandlerList.unregisterAll((Listener) this.phase);
            }
        }

        if (phase instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) phase, CoreAPI.getPlugin());
        }

        this.phase = phase;
    }

    public void cleanPlayer(Player player) {
        if (participants.containsKey(player.getUniqueId())) {
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }

            player.setHealth(20.0);
            player.setFoodLevel(20);
            player.setLevel(0);
            player.setExp(0);
            player.getInventory().setArmorContents(null);
            player.getInventory().clear();
            player.updateInventory();
            player.setGameMode(GameMode.SURVIVAL);
            player.setAllowFlight(false);
            player.setFlying(false);
        }
    }


    public void teleportToLobby(Player player) {
        InvadersConfig config = Invaders.getInstance().getConfiguration();

        if (config == null || config.getSpawn() == null) {
            player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        } else {
            player.teleport(config.getSpawn().getLocation());
        }
    }

    public void teleportAllToLobby() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            teleportToLobby(player);
        }
    }
}
