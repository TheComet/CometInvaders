package co.thecomet.invaders.game.phases;

import co.thecomet.invaders.game.AbstractPhase;
import co.thecomet.invaders.game.Phase;

public class EndGamePhase extends AbstractPhase {
    @Override
    public void tick() {
        // Results
    }

    @Override
    public Phase next() {
        return new PreGamePhase();
    }
}
