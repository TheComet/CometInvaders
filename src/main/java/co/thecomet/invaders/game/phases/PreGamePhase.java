package co.thecomet.invaders.game.phases;

import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.invaders.Invaders;
import co.thecomet.invaders.config.InvadersMapConfig;
import co.thecomet.invaders.game.AbstractPhase;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.GameSession;
import co.thecomet.invaders.game.Phase;
import co.thecomet.invaders.game.ui.ClassSelectionMenu;
import co.thecomet.invaders.game.ui.MapSelectionMenu;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapLoadException;
import co.thecomet.minigameapi.world.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class PreGamePhase extends AbstractPhase implements Listener {
    private final int MIN_PLAYERS = 16;
    private final int COUNTER_START = 60;
    private final int COUNTER_LOAD_MAP = 10;

    private int count = COUNTER_START;

    private MapSelectionMenu mapMenu;
    private ClassSelectionMenu classMenu;

    private LoadedMap<InvadersMapConfig> map;
    private UUID mapUuid;

    public PreGamePhase() {
        mapMenu = new MapSelectionMenu(GameLoop.getInstance().getMapManager());
        classMenu = new ClassSelectionMenu();

        GameLoop.getInstance().getParticipants().values().stream().forEach(participant -> participant.clean());
    }

    @Override
    public void tick() {
        int playerMinOverride = Invaders.getInstance().getConfiguration().getPlayerMinOverride();
        if (Bukkit.getOnlinePlayers().size() < (playerMinOverride > 0 ? playerMinOverride : MIN_PLAYERS) && count == COUNTER_START) {
            return;
        }

        MapManager<InvadersMapConfig> manager = GameLoop.getInstance().getMapManager();

        if (manager.getMaps().size() == 0) {
            return;
        }

        if (map != null && manager.isUpdating()) {
            manager.setUpdating(false);
        } else if (map == null && !manager.isUpdating()) {
            manager.setUpdating(true);
        } else if (map == null && count == COUNTER_LOAD_MAP) {
            map = mapMenu.getWinningMap();

            manager.setCurrent(map);

            try {
                manager.loadSelectedMap(map);

                for (Player player : Bukkit.getOnlinePlayers()) {
                    MessageFormatter.sendInfoMessage(player, map.getMapName() + " has won the vote!");
                }

                mapUuid = Bukkit.getWorld(map.getWorld().getName()).getUID();
            } catch (MapLoadException e) {
                setComplete();
                return;
            }
        }

        count--;

        if (count == 0) {
            setComplete();
        }
    }

    @Override
    public Phase next() {
        return mapUuid != null ? new StartingPhase(new GameSession(map, mapUuid)) : new PreGamePhase();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        initPlayerInventory(event.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        mapMenu.removeVote(event.getPlayer());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(triggerMenuItem(event.getPlayer(), event.getItem()));
    }

    @EventHandler
    public void onGamemodeChange(PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode() == GameMode.SURVIVAL) {
            initPlayerInventory(event.getPlayer());
        }
    }

    public void initPlayerInventory(Player player) {
        player.getInventory().clear();
        player.getInventory().setItem(0, mapMenu.getItem().getItemStack());
        player.getInventory().setItem(1, classMenu.getItem().getItemStack());
        player.updateInventory();
    }

    public boolean triggerMenuItem(Player player, ItemStack stack) {
        if (stack == null || stack.getData().getItemType() == Material.AIR) {
            return false;
        }

        if (mapMenu.getItem().getItemStack().equals(stack)) {
            mapMenu.getItem().onClick(player);
            return true;
        }

        if (classMenu.getItem().getItemStack().equals(stack)) {
            classMenu.getItem().onClick(player);
            return true;
        }

        return false;
    }
}
