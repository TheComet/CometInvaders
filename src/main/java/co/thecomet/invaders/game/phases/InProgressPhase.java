package co.thecomet.invaders.game.phases;

import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.invaders.entities.ControlledArmorStand;
import co.thecomet.invaders.game.AbstractPhase;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.GameSession;
import co.thecomet.invaders.game.Phase;
import co.thecomet.invaders.game.players.Participant;
import co.thecomet.invaders.game.team.Team;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

import java.util.List;

public class InProgressPhase extends AbstractPhase implements Listener {
    private GameSession session;

    public InProgressPhase(GameSession session) {
        this.session = session;
    }

    @Override
    public void tick() {
        // Start Game, Play Game, End
    }

    @Override
    public Phase next() {
        return null;
    }

    @EventHandler
    public void onVehicleExit(VehicleExitEvent event) {
        if (event.getVehicle() instanceof ControlledArmorStand) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
            event.setCancelled(true);
        }
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Participant participant = GameLoop.getInstance().getParticipants().get(event.getPlayer().getUniqueId());
        if (participant.getTeam() != null) {
            participant.getTeam().getMembers().remove(participant.getUuid());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Participant participant = GameLoop.getInstance().getParticipants().get(event.getPlayer().getUniqueId());

        Team team = null;
        for (Team t : session.getTeams()) {
            if (team == null || t.getMembers().size() < team.getMembers().size()) {
                team = t;
            }
        }

        team.getMembers().add(participant.getUuid());
        participant.setTeam(team);
        MessageFormatter.sendInfoMessage(event.getPlayer(), "You are on " + team.getName() + " team.");
    }
}
