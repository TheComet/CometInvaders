package co.thecomet.invaders.game.phases;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.invaders.entities.ControlledArmorStand;
import co.thecomet.invaders.game.AbstractPhase;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.game.GameSession;
import co.thecomet.invaders.game.Phase;
import co.thecomet.invaders.game.players.Participant;
import co.thecomet.invaders.game.team.Team;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

import java.util.List;
import java.util.UUID;

public class StartingPhase extends AbstractPhase implements Listener {
    public List<UUID> loaded = Lists.newArrayList();
    private GameSession session;

    public StartingPhase(GameSession session) {
        this.session = session;
        session.getMap().getConfig().getTeamSettings().entrySet().stream().forEach(entry -> session.getTeams().add(new Team(entry.getKey(), entry.getValue())));

        int index = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (index >= session.getTeams().size()) {
                index = 0;
            }

            Team team = session.getTeams().get(index);
            index++;

            team.getMembers().add(player.getUniqueId());
            Participant participant = GameLoop.getInstance().getParticipants().get(player.getUniqueId());
            participant.setTeam(team);
            MessageFormatter.sendInfoMessage(player, "You are on " + team.getName() + " team.");
        }
    }

    @Override
    public void tick() {
        Bukkit.getOnlinePlayers().stream().filter(player -> !loaded.contains(player.getUniqueId())).forEach(player -> {
            Participant participant  = GameLoop.getInstance().getParticipants().get(player.getUniqueId());
            if (participant != null) {
                for (JsonLocation spawn : participant.getTeam().getSettings().getSpawnPoints()) {
                    boolean found = false;
                    if (!session.getAssignedTeamSpawns().containsValue(spawn)) {
                        participant.setSpawnLocation(spawn);
                        session.getAssignedTeamSpawns().put(participant.getUuid(), spawn);
                        found = true;
                    }

                    if (!found) {
                        JsonLocation defaultTeamSpawn = participant.getTeam().getSettings().getDefaultTeamSpawn();
                        participant.setSpawnLocation(defaultTeamSpawn == null ? new JsonLocation(Bukkit.getWorld(session.getMap().getWorld().getName()).getSpawnLocation()) : defaultTeamSpawn);
                    }
                }

                participant.spawn();
                participant.prepareNewShip();
                loaded.add(player.getUniqueId());
            }
        });
    }

    @Override
    public Phase next() {
        return new InProgressPhase(session);
    }

    @EventHandler
    public void onVehicleExit(VehicleExitEvent event) {
        if (event.getVehicle() instanceof ControlledArmorStand) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Participant participant = GameLoop.getInstance().getParticipants().get(event.getPlayer().getUniqueId());
        if (participant.getTeam() != null) {
            participant.getTeam().getMembers().remove(participant.getUuid());
        }
        session.getAssignedTeamSpawns().remove(participant.getUuid());
        loaded.remove(participant.getUuid());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Participant participant = GameLoop.getInstance().getParticipants().get(event.getPlayer().getUniqueId());

        Team team = null;
        for (Team t : session.getTeams()) {
            if (team == null || t.getMembers().size() < team.getMembers().size()) {
                team = t;
            }
        }

        team.getMembers().add(participant.getUuid());
        participant.setTeam(team);
        MessageFormatter.sendInfoMessage(event.getPlayer(), "You are on " + team.getName() + " team.");
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getFrom().getX() != event.getTo().getX() || event.getFrom().getY() != event.getTo().getY() || event.getFrom().getZ() != event.getTo().getZ()) {
            event.setCancelled(true);
        }
    }
}
