package co.thecomet.invaders.game.phases;

import co.thecomet.invaders.game.AbstractPhase;
import co.thecomet.invaders.game.Phase;

public class MaintenancePhase extends AbstractPhase {
    @Override
    public void tick() {
        // Is There Really Anything To Do Here?
    }

    @Override
    public Phase next() {
        return new PreGamePhase();
    }
}
