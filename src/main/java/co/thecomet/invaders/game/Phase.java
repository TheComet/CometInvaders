package co.thecomet.invaders.game;

public interface Phase {
    public void tick();

    public Phase next();

    public boolean isComplete();

    public void setComplete();
}
