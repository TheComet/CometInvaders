package co.thecomet.invaders;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CorePlugin;
import co.thecomet.core.moderation.commands.TeleportCommands;
import co.thecomet.invaders.config.InvadersConfig;
import co.thecomet.invaders.game.GameLoop;
import co.thecomet.invaders.utils.texturepack.TextureEnforcer;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;

public class Invaders extends CorePlugin {
    @Getter
    private static Invaders instance;
    @Getter
    private InvadersConfig configuration;
    @Getter
    private TextureEnforcer textureEnforcer;

    @Override
    public void enable() {
        instance = this;
        loadConfiguration();
        setupLobby();

        if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            textureEnforcer = new TextureEnforcer();
        }

        new TeleportCommands(true, Rank.MOD, true, Rank.ADMIN, false, null, false, null, true, Rank.ADMIN);
        Bukkit.getScheduler().runTaskTimer(this, new GameLoop(), 20, 20);
    }

    @Override
    public String getType() {
        return "INVADERS";
    }

    public void loadConfiguration() {
        this.configuration = JsonConfig.load(new File(getDataFolder(), "config.json"), InvadersConfig.class);
    }

    public void saveConfiguration() {
        if (this.configuration != null) {
            this.configuration.save(new File(getDataFolder(), "config.json"));
        }
    }

    private void setupLobby() {
        World world = Bukkit.getWorlds().get(0);
        world.setAutoSave(false);
        world.setTime(6000);
        world.setGameRuleValue("doDaylightCycle", String.valueOf("false"));
        world.setGameRuleValue("doFireTick", String.valueOf("false"));
        world.setGameRuleValue("mobGriefing", String.valueOf("false"));
    }
}
